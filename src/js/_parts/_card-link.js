if ($('.card-link').length > 0) {
	if ($(window).width() < 1200) {
		cardLinkHidden(180)
	}
}

function cardLinkHidden(val) {
	$('.card-link').each(function() {
		const _this = $(this);
		const card = _this.parents('.card');
		const textItemHeight = card.find('.card-border__text').height();

		if (textItemHeight < val) {
			_this.hide();
		}
	})
}