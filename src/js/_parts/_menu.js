let menu = $('.js-menu'),
	menuContent = $('.js-menuContent'),
	body = $('body');

$('.js-openMenu').on('click', function() {
	let _this = $(this);

	menu.toggleClass('is-open');
	_this.toggleClass('is-active');
	body.toggleClass('overflow_hidden');

	if (menuContent.hasClass('is-show')) {
		menuContent.removeClass('is-show');
	} else {
		setTimeout( function () {
			menuContent.toggleClass('is-show');
		}, 100)
	}
});