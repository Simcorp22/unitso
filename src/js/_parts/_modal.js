import magnificPopup from 'magnific-popup';
const body = $('body');

function modalOpen(src, type){
    $.magnificPopup.open({
        items: {
            src: src,
            type: type
        },
        closeBtnInside: true,
        callbacks: {
            beforeOpen: function() {
                $('html').addClass('mfp-open');
            },
			beforeClose: function(){
                $('html').removeClass('mfp-open');
            },
        }
    });
}

$(document).on('click', '.js-modalLink', function(event) {
    const src = $(this).data('mfp-src');
    const type = $(this).data('mfp-ajax') || 'inline';
    const _this = $(this);

    event.preventDefault();

    modalOpen(src, type);

    if (_this.hasClass('card-link')) {
        const textCard = _this.parents('.card').find('.card-border__text').html();
        const imageCard = _this.parents('.card').find('.card-icon').html();
        const descriptionCard = _this.parents('.card').find('.card-text').html();

        $('.modal-card').html(textCard);
        $('.js-modalCardImage').html(imageCard);
        $('.js-modalCardDescription').html(descriptionCard);
    }
});

$(document).on('click', '.js-modalClose', (event) => {
	event.preventDefault();
	$.magnificPopup.close();
});
