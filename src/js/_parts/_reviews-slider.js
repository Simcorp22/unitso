if($('.js-reviewsSlider').length > 0){
	let ww = $(window).width();
	let slides = 4;
	const reviewsSliderAdaptive = $('.js-reviewsSlider .swiper-wrapper');

	if (ww < 768) {
		$('.js-reviewsSlider .card').each(function() {
			reviewsSliderAdaptive.append($(this));
		})

		$('.js-reviewsSlider .swiper-slide').remove();

		let col = 3;
		let wrap = $('.js-reviewsSlider .swiper-wrapper');

		while(wrap.children('div:not(.swiper-slide)').length){
			wrap.children('div:not(.swiper-slide):lt('+col+')').wrapAll('<div class="swiper-slide">');	
		}

		slides = 1;
	}

	if (ww >= 768 && ww < 1200) {
		$('.js-reviewsSlider .card').each(function() {
			reviewsSliderAdaptive.append($(this));
		})

		$('.js-reviewsSlider .swiper-slide').remove();

		let col = 2;
		var wrap = $('.js-reviewsSlider .swiper-wrapper');
		while(wrap.children('div:not(.swiper-slide)').length){
			wrap.children('div:not(.swiper-slide):lt('+col+')').wrapAll('<div class="swiper-slide">');	
		}

		slides = 2;
	}

	let reviewsSlider = new Swiper('.js-reviewsSlider .swiper-container', {
		slidesPerView: slides,
		speed: 600,
		// autoHeight: true,
		slidesPerGroup: slides,
		// autoplay: {
		// 	delay: 6000,
		// 	disableOnInteraction: false,
		// },
		navigation: {
			nextEl: '.js-reviewsSlider .swiper-next',
			prevEl: '.js-reviewsSlider .swiper-prev',
		},
	});
}