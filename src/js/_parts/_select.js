window.selectInit = function() {
	$('.js-select2Single').each(function(){
		$(this).select2({
			theme: 'default form-select2',
			minimumResultsForSearch: 100000,
			placeholder:  $(this).data('placeholder'),
			width: '100%',
			language: {
				noResults: function(){
					return $(this).data('noresult-title');
				}
			},
		});

		if ($(this).parents('.js-validate').length > 0) {
			$(this).on('select2:select', function(event) {
				$(event.currentTarget).valid();
			});
		}
		
	});
}

$(window).on('load', function() {
	selectInit();
})