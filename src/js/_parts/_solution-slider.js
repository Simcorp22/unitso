if($('.js-solutionSlider').length > 0){
	let solutionSlider = new Swiper('.js-solutionSlider .swiper-container', {
		speed: 600,
		// autoHeight: true,
		navigation: {
			nextEl: '.js-solutionSlider .swiper-next',
			prevEl: '.js-solutionSlider .swiper-prev',
		},
	});
}