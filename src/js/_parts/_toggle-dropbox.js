$('.js-toggleDropBox').on('click', function() {
	let _this = $(this);

	_this.toggleClass('is-active');

	$( ".js-dropBoxBody" ).slideToggle();
})