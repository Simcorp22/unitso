$.extend($.validator.messages, {
	required: "This field is required.",
		remote: "Please fix this field.",
		email: "Please enter a valid email address.",
		url: "Please enter a valid URL.",
		date: "Please enter a valid date.",
		dateISO: "Please enter a valid date (ISO).",
		number: "Please enter a valid number.",
		digits: "Please enter only digits.",
		equalTo: "Please enter the same value again.",
		maxlength: $.validator.format( "Please enter no more than {0} characters." ),
		minlength: $.validator.format( "Please enter at least {0} characters." ),
		rangelength: $.validator.format( "Please enter a value between {0} and {1} characters long." ),
		range: $.validator.format( "Please enter a value between {0} and {1}." ),
		max: $.validator.format( "Please enter a value less than or equal to {0}." ),
		min: $.validator.format( "Please enter a value greater than or equal to {0}." ),
});

$('.js-validate').each((index, form) => {
	$(form).validate({
		ignore: ':hidden',
	    errorElement: 'span',
	    errorClass: 'label-error',
	    highlight: function(element) {
			setTimeout(() => {
				if(!$(element).attr('disabled')){
					$(element).parent().addClass('is-error');

					if($(element).attr('type') == 'checkbox' || $(element).hasClass('js-select2Single') || $(element).attr('type') == 'radio'){
						const parent = $(element).parent();

						parent.append(parent.find('.label-error'));
					}
				}
			});
        },
        unhighlight: function(element) {
			setTimeout(() => {
				if(!$(element).attr('disabled')){
					if($(element).val() != '') $(element).parent().addClass('is-success');
					$(element).parent().removeClass('is-error');
				}
			});
        }
	});

	$('input, textarea, select', $(form)).on('focus change keyup', function() {
		const valid = $(form).validate().checkForm();
		const submit = $(form).find('[type="submit"]');

		setTimeout(() => {
	        if(valid){
				submit.prop('disabled', false);
	        }
	        else {
				submit.prop('disabled', true);
	        }
	    }, 100);
    });
});

/*
 * Валидация телефона
//  */
// $.validator.addMethod('phonecustom', function(value, element) {
// 	return value.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
// }, 'Please specify a valid phone number');

// $.validator.addClassRules('js-phone', {
// 	phonecustom: true
// });