if ($('.js-videoAbout').length > 0) {
	let ww = $(window).width();

	$(window).on('load', function() {
		const videoAbout = $('.js-videoAbout');
		const videoAboutWrap = $('.js-videoAboutWrap').offset().left;

		if (ww < 1440 && ww > 1199) {
			videoAbout.css('margin-left', -videoAboutWrap).css('margin-right', -videoAboutWrap);
		}
	})
}