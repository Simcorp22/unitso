import './vendor';

import './_parts/_reviews-slider.js';
// import './_parts/_video-about.js';
import './_parts/_toggle-dropbox.js';
import './_parts/_modal.js';
import './_parts/_validate.js';
import './_parts/_select.js';
import './_parts/_menu.js';
import './_parts/_solution-slider.js';
import './_parts/_card-link.js';
